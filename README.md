# layui table add radio
layui数据表格加单选框
修改layui table.js模块源码方式

#2017.10.12 18:00



1. 添加table.radioStatus,该方法可获取到表格单选框选中行相关数据 

var radioStatus = table.radioStatus('idTest'); //test即为基础参数id对应的值

console.log(radioStatus.data) //获取选中行的数据

console.log(radioStatus.data.length) //获取选中行数量，可作为是否有选中行的条件

#2017.11.10 20:00
1. 修复页面有有两个以上数据表格时，只渲染最后一个表格的问题
2. 根据layui社区 发如雪http://fly.layui.com/u/7600656/的建议进行修改：清空单选，添加单选值
